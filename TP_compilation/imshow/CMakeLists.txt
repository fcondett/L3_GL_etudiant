cmake_minimum_required( VERSION 3.0 )
project( imshow )
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -Wall -Wextra -O2" )

find_package( OpenCV REQUIRED )

# programme principal
add_executable( imshow.out imshow.cpp 
    imshow.cpp )
target_link_libraries( imshow.out ${OpenCV_LIBS} )

