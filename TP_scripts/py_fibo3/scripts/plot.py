import numpy
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import fibo, sinus

xs = numpy.arange(0, 1, 0.01)
ys = [sinus.calc_sin(2,0.25,x) for x in xs]
plt.plot(xs, ys)
plt.xlabel('x')
plt.ylabel('calc_sinus(2,0.25,x)')
plt.grid()
plt.savefig('plot_sinus.png')
plt.clf()

