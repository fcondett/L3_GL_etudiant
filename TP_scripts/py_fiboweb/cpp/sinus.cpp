#include <math.h>
#define PI 3.14159265
float calc_sin(float a, float b, float x) {
    return sin(2 * PI * (a * x + b));
}

#include <pybind11/pybind11.h>
PYBIND11_PLUGIN(sinus) {
    pybind11::module m("sinus");
    m.def("calc_sin", &calc_sin);
    return m.ptr();
}
