#include "Fibo.hpp"
#include <cassert>
#include <string>

int fibo(int n, int f0, int f1) {
	if(f0 < 0 || f1 < f0)
			throw std::string("overflow");
    return n<=0 ? f0 : fibo(n-1, f1, f1+f0);
}

