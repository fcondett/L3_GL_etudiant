#include "Fibo.hpp"
#include <CppUTest/CommandLineTestRunner.h>
#include <string>

TEST_GROUP(GroupFibo) { };
TEST(GroupFibo, test_fibo_5) {
    int result[5] {0,1,1,2,3};
    for(int i=0; i<5; i++)
    {
		CHECK_EQUAL(fibo(i), result[i]);
	}
}

TEST(GroupFibo, test_fibo_exception) { 
    CHECK_THROWS(std::string, fibo(49));
}
