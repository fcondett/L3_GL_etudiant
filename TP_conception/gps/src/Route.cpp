#include "Route.hpp"

Route::Route(std::string ville1, std::string ville2, int dist): villeA_(ville1), villeB_(ville2), distance_(dist)
{
}

bool Route::operator==(const Route & r) const {

    // TODO
	if(villeA_ == r.villeA_ && villeB_ == r.villeB_ && distance_ == r.distance_)
		return true;
    return false;
}

bool Route::operator!=(const Route & r) const {

    // TODO
	if(villeA_ != r.villeA_ || villeB_ != r.villeB_ || distance_ != r.distance_)
		return true;
    return false;
}

