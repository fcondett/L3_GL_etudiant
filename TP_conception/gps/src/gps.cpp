/// \mainpage gps : calculateur de chemin "au plus court"
/// 
/// Permet de calculer le chemin le plus court entre deux villes, à partir d'un
/// fichier CSV contenant les routes "villeA villeB distance". Écrit, en sortie,
/// un fichier graphviz permettant de générer un graphique.
///

#include "Chemin.hpp"
#include <exception>
#include <fstream>

using namespace std;

int main(int argc, char ** argv) {

    if (argc != 5) {
        cerr << "usage: " << argv[0] 
            << " <input csv> <output dot> <city1> <city2>\n";
        exit(-1);
    }

    try {

        // TODO importer le fichier CSV
        string filename=argv[1];
        ifstream is;
		is.open(filename, ios::in);
		if(!is.is_open()){
			cout << "erreur de lecture du fichier " << filename << endl;
			return -2;
		}
		
		Chemin chemin;
		chemin.importerCsv(is);
		cout<<"fin du fichier"<<endl;
		is.close();
		
        // TODO exporter le fichier dot correspondant
		string filename2=argv[2];
        ofstream os;
		os.open(filename2, ios::out);
		if(!os.is_open()){
			cout << "erreur de lecture du fichier " << filename2 << endl;
			return -2;
		}
		chemin.exporterDot(os, argv[3], argv[4]);
		
    }
    catch (const string & msg) {
        cerr << msg << endl;
    }
    catch (const exception & e) {
        cerr << e.what() << endl;
    }

    return 0;
}

