#ifndef ROUTE_HPP_
#define ROUTE_HPP_

#include <string>

// route définie par deux villes et leur distance
struct Route {

    std::string villeA_;

    std::string villeB_;

    int distance_;

	Route(std::string ville1, std::string ville2, int dist);
	
    // égalité (teste ville A, ville B et distance)
    bool operator==(const Route & r) const;

    // différence (teste ville A, ville B et distance)
    bool operator!=(const Route & r) const;
};

#endif

