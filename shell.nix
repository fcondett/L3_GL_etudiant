with import<nixpkgs> {};

stdenv.mkDerivation {

  name = "L3_GL";

  buildInputs = [
    doxygen
    python3Packages.sphinx

    boost
    cmake
    cppcheck
    cpputest
    eigen
    gdb
    glog
    gnome2.gtkmm
    gnome3.gtkmm
    gnuplot
    imagemagick
    opencv
    pkgconfig
    poco
    sqlitebrowser
    valgrind
    vlc
  ];

  # ~/.bashrc : export PS1="\W \$ "
  shellHook = ''
    export PS1="[\W] \$ "
  '';
}

